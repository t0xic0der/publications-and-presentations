# Interview with The Open Group

<!-- We'll use this space to collaborate on questions and answers that will eventually comprise the published interview—bbehr -->

* Who is The Open Group? What's your mission?

* How long has The Open Group been using GitLab? What GitLab features do you find most helpful and useful?

* Why did you choose GitLab?

* What has using GitLab helped your organization accomplish?

* What's on the horizon for The Open Group?

* What's the best way for people interested in your mission to connect and collaborate with you?
