# GitLab Open Source Partner Presentations

A running list of presentations by and about GitLab Open Source Partners

## Webcasts

### 2023

[Using GitLab to power citizen journalism](https://youtu.be/4wIg2M1EoHI)

### 2022

* [KDE empowers open source community with GitLab](https://youtu.be/aLDFlXSI0Qs)

### 2021

* [VLC migration to GitLab ](https://youtu.be/B4fP9OA6Dw4)
