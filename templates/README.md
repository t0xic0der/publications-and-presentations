# Templates
This folder contains templates members of the GitLab Open Source Partners program can use in support of various [program activities](https://about.gitlab.com/solutions/open-source/partners/).
