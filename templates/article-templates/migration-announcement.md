# Migration announcement article template

## About
We typically publish migration announcements when a community has successfully migrated some significant aspect of its infrastructure to GitLab. For partners, these articles serve as opportunities to attract new users to their projects by drawing attention to activities on GitLab. For GitLab, these articles serve as opportunities to have well-known and well-respected open source projects explain GitLab's benefits to their peers. 

## Project details

* Project name: 
* Project founding date (approximate): 
* Estimated number of contributors: 

## Questions to answer

* What, specifically, has your community recently finished migrating to GitLab?
* What motivated the community to begin seeking alternatives to the way it had been working, or to a tool it had previously been using?
* What challenges was the community facing that its current tooling couldn't overcome? In what ways was its previous tooling limiting the project?
* Why did the community choose to migrate this important project or infrastructure to GitLab?
* What does the community hope to accomplish by migrating to GitLab?
* Has the community seen evidence that its migration to GitLab is helping ti achieve its goals?
* How can other GitLab users now assist the community now that it has migrated to GitLab?

## Examples

*  [Come on in! Drupal is moving to GitLab](https://about.gitlab.com/blog/2018/08/16/drupal-moves-to-gitlab/) 
*  [Welcoming OpenCores to GitLab](https://about.gitlab.com/blog/2019/12/03/welcoming-opencores-to-gitlab/)
