# GitLab Open Source Partner Publications
A running list of publications by and about GitLab Open Source Partners

## GitLab blog articles

### 2023

* 2023-01-30: [Start an open source center of excellence in 10 minutes using GitLab](https://about.gitlab.com/blog/2023/01/30/how-start-ospo-ten-minutes-using-gitlab/) | `https://go.gitlab.com/fhKvUa`

### 2021

* 2021-02-18: [How GitLab helped Kali Linux attract a growing number of community contributions](https://about.gitlab.com/blog/2021/02/18/kali-linux-movingtogitlab/)

### 2020

* 2020-09-08: [GNOME: two years after the move to GitLab](https://about.gitlab.com/blog/2020/09/08/gnome-follow-up/)
* 2020-06-29: [Why the KDE community is #movingtogitlab](https://about.gitlab.com/blog/2020/06/29/welcome-kde/)
* 2020-05-15: [Announcing 32/64-bit Arm Runner Support for AWS Graviton2](https://about.gitlab.com/blog/2020/05/15/gitlab-arm-aws-graviton2-solution/)

### 2019

* 2019-12-03: [Welcoming OpenCores to GitLab](https://about.gitlab.com/blog/2019/12/03/welcoming-opencores-to-gitlab/)
* 2019-10-08: [DevOps on the edge: Upcoming collaborations between GitLab and Arm](https://about.gitlab.com/blog/2019/10/08/devops-on-the-edge-a-conversation-about-gitlab-and-arm/)

### 2018

* 2022-08-16: [Come on in! Drupal is moving to GitLab](https://about.gitlab.com/blog/2018/08/16/drupal-moves-to-gitlab/)

## Case studies

* [GitLab accelerates innovation and improves efficiency for Synchrotron SOLEIL](https://about.gitlab.com/customers/synchrotron_soleil/) | `https://go.gitlab.com/HesQjW`
* [How SKA uses GitLab to help construct the world’s largest telescope](https://about.gitlab.com/customers/square_kilometre_array/) | `https://go.gitlab.com/5nEcqT`
* [Drupal Association eases entry for new committers, speeds implementations](https://about.gitlab.com/customers/drupalassociation/) | `https://go.gitlab.com/q0YkrU`
